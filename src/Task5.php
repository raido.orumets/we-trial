<?php

namespace Raido\Trial;

class Task5
{
    public function run()
    {
?>
        <div><span class="badge badge-secondary">Before</span></div>
        <?php

        $str1 = 'yabadabadoo';
        $str2 = 'yaba';
        if (strpos($str1, $str2)) {
            echo "\"" . $str1 . "\" contains \"" . $str2 . "\"";
        } else {
            echo "\"" . $str1 . "\" does not contain \"" . $str2 . "\"";
        }

        ?>
        <div><span class="badge badge-secondary mt-3">After</span></div>
<?php

        $str1 = 'yabadabadoo';
        $str2 = 'yaba';
        if (strpos($str1, $str2) === false) {
            echo "\"" . $str1 . "\" does not contain \"" . $str2 . "\"";
        } else {
            echo "\"" . $str1 . "\" contains \"" . $str2 . "\"";
        }
    }
}
