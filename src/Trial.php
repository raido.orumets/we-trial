<?php

namespace Raido\Trial;

use Raido\Trial\Task1;
use Raido\Trial\Task2;
use Raido\Trial\Task3;
use Raido\Trial\Task4;
use Raido\Trial\Task5;
use Raido\Trial\Task6;
use Raido\Trial\Task7;
use Raido\Trial\Task8;
use Raido\Trial\Task9;
use Raido\Trial\DbTask1;
use Raido\Trial\DbTask2;

class Trial
{
    private function printHeader()
    {
        require_once(__DIR__ . '/template/header.php');
    }

    private function printFooter()
    {
        require_once(__DIR__ . '/template/header.php');
    }

    private function printHeadline($text, $tag = 'h2')
    {
        echo '<' . $tag . ' class="pt-5">' . $text . '</' . $tag . '>';
    }

    public function run()
    {
        $this->printHeader();

        $this->printHeadline('PHP coding', 'h1');

        $this->printHeadline('Task 1');
        $task = new Task1();
        $task->run();

        $this->printHeadline('Task 2');
        $task = new Task2();
        $task->run();

        $this->printHeadline('Task 3');
        $task = new Task3();
        $task->run();

        $this->printHeadline('Task 4');
        $task = new Task4();
        $task->run();

        $this->printHeadline('Task 5');
        $task = new Task5();
        $task->run();

        $this->printHeadline('Task 6');
        $task = new Task6();
        $task->run();

        $this->printHeadline('Task 7');
        $task = new Task7();
        $task->run();

        $this->printHeadline('Task 8');
        $task = new Task8();
        $task->run();

        $this->printHeadline('Task 9');
        $task = new Task9();
        $task->run();

        $this->printHeadline('MySQL/MariaDB', 'h1');

        $this->printHeadline('Task 1');
        $task = new DbTask1();
        $task->run();

        $this->printHeadline('Task 2');
        $task = new DbTask2(getenv('DB_HOST'), getenv('DB_PORT'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
        $task->run();

        $this->printFooter();
    }
}
