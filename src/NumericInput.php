<?php

namespace Raido\Trial;

use Raido\Trial\TextInput;

class NumericInput extends TextInput
{
    public function add($value)
    {
        if (is_numeric($value)) {
            $this->values[] = $value;
        }
    }
}
