<?php

namespace Raido\Trial;

class TextInput
{
    public array $values = [];

    public function add($value)
    {
        $this->values[] = $value;
    }

    public function getValue()
    {
        return implode('', $this->values);
    }
}
