<?php

namespace Raido\Trial;

use mysqli;

class DbTask2
{
    private $db;

    public function __construct($dbHost, $dbPort, $dbUsername, $dbPassword, $dbDatabase)
    {
        $this->db = new mysqli($dbHost . ':' . $dbPort, $dbUsername, $dbPassword, $dbDatabase);
    }

    public function __destruct()
    {
        $this->db->close();
    }

    private function getListOfJobs()
    {
        $rows = [];
        $sql = "SELECT 
                    job_id, 
                    COUNT(*) as count 
                FROM 
                    employees 
                GROUP BY 
                    job_id
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function getMaxSalaryByJob($jobId)
    {
        $sql = "SELECT 
                    job_id, 
                    MAX(salary) as salary
                FROM 
                    employees 
                WHERE 
                    job_id = '" . $jobId . "' 
                LIMIT 1
                ";
        $result = $this->db->query($sql);
        $row = $result->fetch_object();
        return $row;
    }

    private function getAverageSalarAndNumberOfEmployeesByDepartment($departmentId)
    {
        $sql = "SELECT 
                    COUNT(*) as count, 
                    AVG(salary) as salary
                FROM  
                    employees 
                WHERE 
                    department_id = '" . $departmentId . "' 
                LIMIT 1
                ";
        $result = $this->db->query($sql);
        $row = $result->fetch_object();
        return $row;
    }

    private function getListOfEmployeesWithSameJob()
    {
        $rows = [];
        $sql = "SELECT 
                    job_id, 
                    COUNT(*) as count 
                FROM 
                    employees 
                GROUP BY 
                    job_id
                HAVING COUNT(*) > 1
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function getDifferenceBetweenHighestAndLowestSalaries()
    {
        $rows = [];
        $sql = "SELECT 
                    job_id, 
                    MIN(salary) as min_salary,
                    MAX(salary) as max_salary,
                    (MAX(salary) - MIN(salary)) as difference_between_salaries
                FROM 
                    employees 
                GROUP BY 
                    job_id
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function getListOfLowestPaidSalariesByManager()
    {
        $rows = [];
        $sql = "SELECT 
                    manager_id, 
                    MIN(salary) as min_salary
                FROM 
                    employees 
                GROUP BY 
                    manager_id
                ORDER BY 
                    manager_id ASC
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function getListOfDepartmentsAndTotalSalaries()
    {
        $rows = [];
        $sql = "SELECT 
                    department_id, 
                    SUM(salary) as total_salary
                FROM 
                    employees 
                GROUP BY 
                    department_id
                ORDER BY 
                    department_id ASC
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function getListOfJobsAndAverageSalariesExcluding($jobIds)
    {
        $rows = [];
        $sql = "SELECT 
                    job_id,
                    AVG(salary) as average_salary
                FROM  
                    employees
                WHERE 
                    job_id NOT IN ('" . implode("','", $jobIds) . "') 
                GROUP BY
                    job_id
                ORDER BY 
                    job_id ASC
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function getListOfJobsAndSalaryStatisticsByDepartment($departmentId)
    {
        $rows = [];
        $sql = "SELECT 
                    department_id,
                    job_id,
                    MIN(salary) as min_salary,
                    MAX(salary) as max_salary,
                    AVG(salary) as average_salary,
                    SUM(salary) as total_salary
                FROM  
                    employees
                WHERE 
                    department_id = '" . $departmentId . "' 
                GROUP BY
                    job_id
                ORDER BY 
                    job_id ASC
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function getListOfJobsAndMaxSalaryWhereMaxSalaryGte($salary)
    {
        $rows = [];
        $sql = "SELECT 
                    job_id,
                    MAX(salary) as max_salary
                FROM  
                    employees
                GROUP BY
                    job_id
                HAVING 
                    MAX(salary) >= '" . $salary . "'
                ORDER BY 
                    job_id ASC
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function getListOfDepartmentsAndAverageSalaryWhereCountOfEmployeesGt($employeeCount)
    {
        $rows = [];
        $sql = "SELECT 
                    department_id,
                    AVG(salary) as average_salary,
                    COUNT(*) as employee_count
                FROM  
                    employees
                GROUP BY
                    department_id
                HAVING 
                    COUNT(*) > '" . $employeeCount . "'
                ORDER BY 
                    department_id ASC
                ";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function printResult($title, $result)
    {
        echo '<h3 class="mt-3">' . $title . '</h3>';
        echo '<pre>';
        echo json_encode($result, JSON_PRETTY_PRINT);
        echo '</pre>';
    }

    public function run()
    {
        $this->printResult('1) List the number of jobs available in the employees table', $this->getListOfJobs());
        $this->printResult('2) Maximum salary of an employee working as a Programmer (“IT_PROG”)', $this->getMaxSalaryByJob('IT_PROG'));
        $this->printResult('3) Average salary and number of employees working the department 90', $this->getAverageSalarAndNumberOfEmployeesByDepartment(90));
        $this->printResult('4) Number of employees with the same job', $this->getListOfEmployeesWithSameJob());
        $this->printResult('5) Get the difference between the highest and lowest salaries', $this->getDifferenceBetweenHighestAndLowestSalaries());
        $this->printResult('6) Manager ID and the salary of the lowest-paid employee for that manager', $this->getListOfLowestPaidSalariesByManager());
        $this->printResult('7) Department ID and the total salary payable in each department', $this->getListOfDepartmentsAndTotalSalaries());
        $this->printResult('8) Get the average salary for each job ID excluding programmer', $this->getListOfJobsAndAverageSalariesExcluding(['IT_PROG']));
        $this->printResult('9) Total salary, maximum, minimum, average salary of employees (job ID wise), for department ID 90 only', $this->getListOfJobsAndSalaryStatisticsByDepartment(90));
        $this->printResult('10) Get the job ID and maximum salary of the employees where maximum salary is greater than or equal to $4000', $this->getListOfJobsAndMaxSalaryWhereMaxSalaryGte(4000));
        $this->printResult('11) Get the average salary for all departments employing more than 10 employees', $this->getListOfDepartmentsAndAverageSalaryWhereCountOfEmployeesGt(10));
    }
}
