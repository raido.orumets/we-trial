<?php

namespace Raido\Trial;

class Task7
{
    private function getBiggestNr($numbers)
    {
        $length = count($numbers);
        $max = $numbers[0];
        for ($i = 1; $i < $length; $i++) {
            if ($numbers[$i] > $max) {
                $max = $numbers[$i];
            }
        }

        return $max;
    }

    public function run()
    {
        $numbers = [0, 10, 80, 67, 60, 89, 91, 56, 45, 30, 95, 83];

        echo '<pre>';
        print_r($numbers);
        echo '</pre>';
?>
        <div>Biggest nr in array: <span class="badge badge-secondary mt-3"><?php echo $this->getBiggestNr($numbers); ?></span></div>
<?php
    }
}
