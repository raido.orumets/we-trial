<?php

namespace Raido\Trial;

class Task6
{
    private function getPattern($length)
    {
        $string = "*";
        $patternString = "";
        $midPoint = ceil($length / 2);

        for ($i = 1; $i <= $midPoint; $i++) {
            for ($j = 1; $j <= $i; ++$j) {
                $patternString .= $string . " ";
            }
            $patternString .= "\r\n";
        }

        for ($i = $midPoint; $i >= 1; $i--) {
            for ($j = 1; $j < $i; ++$j) {
                $patternString .= $string . " ";
            }
            $patternString .= "\r\n";
        }

        return $patternString;
    }

    public function run()
    {
        echo '<pre>';
        echo $this->getPattern(11);
        echo '</pre>';
    }
}
