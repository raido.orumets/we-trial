<?php

namespace Raido\Trial;

class Task2
{
    public function sumArray($array)
    {
        $sum = 0;
        foreach ($array as $value) {
            if (is_array($value)) {
                $sum += $this->sumArray($value);
            } else {
                $sum += $value;
            }
        }
        return $sum;
    }

    public function run()
    {
        $array = array(0, 1, 2, array(4, 5, array(7, 8)));

        $sum = $this->sumArray($array);

?>
        <span class="badge badge-secondary"><?php echo $sum; ?></span>
<?php
    }
}
