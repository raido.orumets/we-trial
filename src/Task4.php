<?php

namespace Raido\Trial;

use Raido\Trial\TextInput;
use Raido\Trial\NumericInput;

class Task4
{
    public function run()
    {
        $input = new TextInput();
        $input->add('1');
        $input->add('a');
        $input->add('0');
?>
        <div>TextInput: <span class="badge badge-secondary"><?php echo $input->getValue(); ?></span></div>
        <?php
        $input = new NumericInput();
        $input->add('1');
        $input->add('a');
        $input->add('0');
        ?>
        <div>NumericInput: <span class="badge badge-secondary"><?php echo $input->getValue(); ?></span></div>
<?php
    }
}
