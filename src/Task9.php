<?php

namespace Raido\Trial;

class Task9
{
    private function showDartGameScoreBefore($scores)
    {
        $averages = [];
        $places = [];
        foreach ($scores as $player => $values) {
            $averages[$player] = $values[0] + $values[1] + $values[2] / 3;
        }
        arsort($averages);
        $place = 0;
        $lastscore = null;
        foreach ($averages as $player => $average) {
            if ($lastscore and $lastscore = $average) {
                echo sprintf("Place %s (tie) - %s\n", $place, $player);
            } else {
                $place++;
                echo sprintf("Place %s - %s\n", $place, $player);
            }
            $lastscore = $average;
        }
    }

    private function showDartGameScoreAfter($scores)
    {
        $averages = [];
        $places = [];
        foreach ($scores as $player => $values) {
            $averages[$player] = $values[0] + $values[1] + $values[2] / 3;
        }
        arsort($averages);
        $place = 0;
        $lastscore = null;
        foreach ($averages as $player => $average) {
            if ($lastscore and $lastscore == $average) {
                echo sprintf("Place %s (tie) - %s (%s)\n", $place, $player, $average);
            } else {
                $place++;
                echo sprintf("Place %s - %s (%s)\n", $place, $player, $average);
            }
            $lastscore = $average;
        }
    }

    public function run()
    {
        $scores = [
            "John" => [7, 8, 7],
            "Sue" => [10, 8, 4],
            "Tommy" => [8, 8, 7],
            "Mary" => [7, 6, 6],
        ];

        echo '<h3>Before</h3>';
        echo '<pre>';
        $this->showDartGameScoreBefore($scores);
        echo '</pre>';

        echo '<h3>After</h3>';
        echo '<pre>';
        $this->showDartGameScoreAfter($scores);
        echo '</pre>';
    }
}
