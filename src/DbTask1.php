<?php

namespace Raido\Trial;

class DbTask1
{

    public function run()
    {
        echo '<pre>';
?>
        CREATE TABLE `books` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `title` varchar(100) NOT NULL DEFAULT '',
        `total_amount` int(11) DEFAULT NULL,
        `created_at` datetime DEFAULT NULL,
        `modified_at` datetime DEFAULT NULL,
        `deleted_at` datetime DEFAULT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

        INSERT INTO `books` (`id`, `title`, `total_amount`, `created_at`, `modified_at`, `deleted_at`)
        VALUES
        (1,'Mysteries of Java',NULL,'2023-01-09 12:27:59','2023-01-09 12:27:59',NULL),
        (2,'The Big Rewrite”, “Design patterns',NULL,'2023-01-09 12:27:59','2023-01-09 12:27:59',NULL),
        (3,'Design patterns',NULL,'2023-01-09 12:27:59','2023-01-09 12:27:59',NULL),
        (4,'Inversion of control',NULL,'2023-01-09 12:27:59','2023-01-09 12:27:59',NULL),
        (5,'Why my code smells?',NULL,'2023-01-09 12:27:59','2023-01-09 12:27:59',NULL);



        CREATE TABLE `customers` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `firstname` varchar(50) DEFAULT '',
        `lastname` varchar(50) NOT NULL DEFAULT '',
        `phone` varchar(20) DEFAULT NULL,
        `created_at` datetime DEFAULT NULL,
        `modified_at` datetime DEFAULT NULL,
        `deleted_at` datetime DEFAULT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

        INSERT INTO `customers` (`id`, `firstname`, `lastname`, `phone`, `created_at`, `modified_at`, `deleted_at`)
        VALUES
        (1,'Mary','Sue','5012345','2023-01-09 12:27:59','2023-01-09 12:27:59',NULL),
        (2,'Alan','Smith','5123456','2023-01-09 12:27:59','2023-01-09 12:27:59',NULL),
        (3,'Joe','Goodspeed','5234567','2023-01-09 12:27:59','2023-01-09 12:27:59',NULL),
        (4,'Nicky','Jones','5345678','2023-01-09 12:27:59','2023-01-09 12:27:59',NULL);



        CREATE TABLE `rentals` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `customer_id` int(11) NOT NULL,
        `book_id` int(11) NOT NULL,
        `borrowed_at` date NOT NULL,
        `returned_at` date DEFAULT NULL,
        `created_at` datetime DEFAULT NULL,
        `modified_at` datetime DEFAULT NULL,
        `deleted_at` datetime DEFAULT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

        INSERT INTO `rentals` (`id`, `customer_id`, `book_id`, `borrowed_at`, `returned_at`, `created_at`, `modified_at`, `deleted_at`)
        VALUES
        (1,1,1,'2020-02-14',NULL,'2023-01-09 12:27:59','2023-01-09 12:27:59',NULL),
        (2,1,3,'2020-03-02','2020-03-04','2023-01-09 12:31:35','2023-01-09 12:31:35',NULL),
        (3,2,2,'2020-02-17','2020-02-25','2023-01-09 12:32:26','2023-01-09 12:32:26',NULL),
        (4,3,3,'2020-03-02',NULL,'2023-01-09 12:33:34','2023-01-09 12:33:34',NULL),
        (5,3,4,'2020-03-04',NULL,'2023-01-09 12:33:34','2023-01-09 12:33:34',NULL),
        (6,4,5,'2020-03-07',NULL,'2023-01-09 12:33:34','2023-01-09 12:33:34',NULL);
<?php
        echo '</pre>';
    }
}
