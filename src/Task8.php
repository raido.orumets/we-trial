<?php

namespace Raido\Trial;

class Task8
{
    private function printMultiplicationTable($input)
    {
?>
        <table class="table table-striped" border="1">

            <?php
            for ($r = 1; $r <= $input; $r++) {
                if ($r == 1) {
                    echo "<tr>";
                    for ($c = 0; $c <= $input; $c++) {
                        echo "<td>" . (($c > 0) ? $c : '') . "</td>";
                    }
                    echo  "</tr>";
                }
                echo "<tr>";
                for ($c = 1; $c <= $input; $c++) {
                    if ($c == 1) {
                        echo "<td>" . $r . "</td>";
                    }
                    echo "<td>" . $r * $c . "</td>";
                }
                echo  "</tr>";
            }
            ?>
        </table>
<?php
    }
    public function run()
    {
        $this->printMultiplicationTable(5);
    }
}
