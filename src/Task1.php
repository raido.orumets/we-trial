<?php

namespace Raido\Trial;

class Task1
{
    public function run()
    {
?>
        <ul class="list-group">
            <?php for ($i = 1; $i <= 30; $i++) { ?>
                <li class="list-group-item"><b><?php echo $i; ?></b>
                    <?php
                    if ($i % 3 == 0 && $i % 5 == 0) {
                        echo " - divisible by three and five";
                    } elseif ($i % 3 == 0) {
                        echo " - divisible by three";
                    } elseif ($i % 5 == 0) {
                        echo " - divisible by five";
                    }
                    ?>
                </li>
            <?php } ?>
        </ul>
<?php
    }
}
