<?php

namespace Raido\Trial;

class Task3
{
    public function groupByAuthor($books)
    {
        $array = [];
        foreach ($books as $book => $author) {
            $array[$author][] = $book;
        }
        return $array;
    }

    public function run()
    {
        $books = [
            "Learning PHP" => "John Smith",
            "Understanding relational databases" => "Mary Little",
            "Freelancers" => "Robin Round",
            "I love LISP" => "Mary Little",
            "Python for dummies" => "John Smith",
        ];

        $booksGroupedByAuthor = $this->groupByAuthor($books);

        echo '<pre>';
        print_r($books);
        echo '</pre>';

        echo '<pre>';
        print_r($booksGroupedByAuthor);
        echo '</pre>';
    }
}
