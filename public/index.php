<?php
error_reporting(E_ALL);
session_start();
ob_start();

define('TIMEZONE', 'Europe/Tallinn');
date_default_timezone_set(TIMEZONE);

header("Content-Type: text/html; charset=utf-8");

require __DIR__ . '/../vendor/autoload.php';

use Raido\Trial\Trial;
use Dotenv\Dotenv;

$dotenv = Dotenv::createUnsafeImmutable(__DIR__ . '/../');
$dotenv->load();

$trial = new Trial();
$trial->run();
